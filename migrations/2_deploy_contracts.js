var Storage = artifacts.require('./Storage');
var Upgrade = artifacts.require('./Upgrade');
var Role = artifacts.require('./Role');
var Settings = artifacts.require('./Settings');

var Main = artifacts.require('./Main');

module.exports = function(deployer) {
    //var storage;


// Deploy the Storage contract
    deployer.deploy(Storage)
        // Wait until the storage contract is deployed
        .then(() => Storage.deployed({gas: 4200000,
      gasPrice: web3.toWei("11", "gwei")}))
        
        .then(() => deployer.deploy(Role, Storage.address,{gas: 4200000,
      gasPrice: web3.toWei("11", "gwei")}))

        .then(() => deployer.deploy(Settings, Storage.address,{gas: 4200000,
      gasPrice: web3.toWei("11", "gwei")}))

     
        .then(() => deployer.deploy(Main, Storage.address,{gas: 4200000,
      gasPrice: web3.toWei("11", "gwei")}))

        .then(() => deployer.deploy(Upgrade, Storage.address,{gas: 4200000,
      gasPrice: web3.toWei("11", "gwei")}));


}

