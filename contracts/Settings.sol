pragma solidity 0.4.24;


import "./Base.sol";
import "./interface/StorageInterface.sol";


/// @title Common settings that are used across all spoke contracts, mostly the main pool and the mini pools it creates
/// @author David Rugendyke
contract Settings is Base {




    /// @dev Settings constructor
    constructor(address _StorageAddress) Base(_StorageAddress) public {
        /*** Version ***/
        version = 1;
    }


    /// @dev Initialise after deployment to not exceed the gas block limit
    function init() public onlyOwner {
        // Only set defaults on deployment
        if (!Storage.getBool(keccak256("settings.init"))) {


            Storage.setBool(keccak256("settings.init"), true);

            //allow only latest upgraded contract to access and interact with the storage
            Storage.setBool(keccak256("contract.storage.initialised"), true);

        }
    }


    /*** Getters *****************/

    /// @dev Get the settings init
    function getSettingsInit() public view returns (bool) {
        return Storage.getBool(keccak256("settings.init"));
    }



    /*** Roles *****************/

    /// @dev Get X address role
    function isRole(address _who, string _role) public view returns (bool) {
        return Storage.getBool(keccak256(abi.encodePacked("access.role", _role, _who)));
    }

    

}
