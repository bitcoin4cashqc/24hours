pragma solidity 0.4.24;

import "./Base.sol";
import "./interface/StorageInterface.sol";

/// @title Role Based Access Control for Pool
/// @author David Rugendyke
contract Role is Base {


     /*** Events **************/

    event RoleAdded(
        string _roleName, 
        address _address
    );

    event RoleRemoved(
        string _roleName, 
        address _address
    );

    event OwnershipTransferred(
        address indexed _previousOwner, 
        address indexed _newOwner
    );


    /*** Modifiers ************/

    /// @dev Only allow access from the latest version of the Role contract
    modifier onlyLatestRole() {
        require(address(this) == Storage.getAddress(keccak256(abi.encodePacked("contract.name", "Role"))));
        _;
    }
  
    /*** Constructor **********/
   
    /// @dev constructor
    constructor(address _StorageAddress) Base(_StorageAddress) public {
        // Set the version
        version = 1;
    }

     /**
    * @dev Allows the current owner to transfer control of the contract to a newOwner.
    * @param _newOwner The address to transfer ownership to.
    */
    function transferOwnership(address _newOwner) public onlyLatestRole onlyOwner {
        // Legit address?
        require(_newOwner != 0x0);
        // Check the role exists 
        roleCheck("owner", msg.sender);
        // Remove current role
        Storage.deleteBool(keccak256(abi.encodePacked("access.role", "owner", msg.sender)));
        // Add new owner
        Storage.setBool(keccak256(abi.encodePacked("access.role",  "owner", _newOwner)), true);
    }


    /**** Admin Role Methods ***********/


   /**
   * @dev Give an address access to this role
   */
    function adminRoleAdd(string _role, address _address) onlyLatestRole onlyOwner public {
        roleAdd(_role, _address);
    }

    /**
   * @dev Remove an address access to this role
   */
    function adminRoleRemove(string _role, address _address) onlyLatestRole onlyOwner public {
        roleRemove(_role, _address);
    }


    /**** Internal Role Methods ***********/
   
    /**
   * @dev Give an address access to this role
   */
    function roleAdd(string _role, address _address) internal {
        // Legit address?
        require(_address != 0x0);
        // Only one owner to rule them all
        require(keccak256(abi.encodePacked(_role)) != keccak256("owner"));
        // Add it
        Storage.setBool(keccak256(abi.encodePacked("access.role", _role, _address)), true);
        // Log it
        emit RoleAdded(_role, _address);
    }

    /**
    * @dev Remove an address' access to this role
    */
    function roleRemove(string _role, address _address) internal {
        // Only an owner can transfer their access
        require(!roleHas("owner", _address));
        // Remove from storage
        Storage.deleteBool(keccak256(abi.encodePacked("access.role", _role, _address)));
        // Log it
        emit RoleRemoved(_role, _address);
    }
    
    
}
