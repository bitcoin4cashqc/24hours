pragma solidity 0.4.24;


contract SettingsInterface {
    /// @dev Get the current average block time for the network
    function getSettingsInit() public view returns (bool);

    /// @dev Get the current average block time for the network
    function isRole(address _who, string _role) public view returns (bool);
    
}