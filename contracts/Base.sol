pragma solidity 0.4.24;

import "./interface/StorageInterface.sol";

/// @title Base settings / modifiers for each contract in network
/// @author David Rugendyke
contract Base {


    /**** Properties ************/

    uint8 public version;                                                   // Version of this contract


    /*** Contracts **************/

    StorageInterface Storage = StorageInterface(0);       // The main storage contract where primary persistant storage is maintained


    /*** Modifiers ************/

    /**
    * @dev Throws if called by any account other than the owner.
    */
    modifier onlyOwner() {
        roleCheck("owner", msg.sender);
        _;
    }

    /**
    * @dev Modifier to scope access to sellers
    */
    modifier onlyAdmin() {
        roleCheck("seller", msg.sender);
        _;
    }

    /**
    * @dev Modifier to scope access to sellers
    */
    modifier onlySuperUser() {
        require(roleHas("owner", msg.sender) || roleHas("seller", msg.sender));
        _;
    }

    /**
    * @dev Reverts if the address doesn't have this role
    */
    modifier onlyRole(string _role) {
        roleCheck(_role, msg.sender);
        _;
    }

  
    /*** Constructor **********/
   
    /// @dev Set the main Storage address
    constructor(address _StorageAddress) public {
        // Update the contract address
        Storage = StorageInterface(_StorageAddress);
    }


    /*** Role Utilities */

    /**
    * @dev Check if an address has this role
    * @return bool
    */
    function roleHas(string _role, address _address) internal view returns (bool) {
        return Storage.getBool(keccak256(abi.encodePacked("access.role", _role, _address)));
    }

     /**
    * @dev Check if an address has this role, reverts if it doesn't
    */
    function roleCheck(string _role, address _address) view internal {
        require(roleHas(_role, _address) == true);
    }

    
    
}
