pragma solidity 0.4.24;


import "./Base.sol";
import "./Storage.sol";
//import "./interface/StorageInterface.sol";



/// @title Upgrades for network contracts
/// @author David Rugendyke
contract Upgrade is Base {


  

     /*** Events ****************/

    event ContractUpgraded (
        address indexed _oldContractAddress,                    // Address of the contract being upgraded
        address indexed _newContractAddress,                    // Address of the new contract
        uint256 created                                         // Creation timestamp
    );

    event ContractAdded (
        address indexed _contractAddress,                       // Address of the contract added
        uint256 created                                         // Creation timestamp
    );


    /*** Constructor ***********/    

    /// @dev Upgrade constructor
    constructor(address _StorageAddress) Base(_StorageAddress) public {
        // Set the version
        version = 1;
    }

    /**** Contract Upgrade Methods ***********/

    /// @param _name The name of an existing contract in the network
    /// @param _upgradedContractAddress The new contracts address that will replace the current one
    /// @param _forceEther Force the upgrade even if this contract has ether in it

    function upgradeContract(string _name, address _upgradedContractAddress, bool _forceEther) onlyOwner external {
        // Get the current contracts address
        address oldContractAddress = Storage.getAddress(keccak256(abi.encodePacked("contract.name", _name)));
        // Check it exists
        require(oldContractAddress != 0x0);
        // Check it is not the contract's current address
        require(oldContractAddress != _upgradedContractAddress);
        // Firstly check the contract being upgraded does not have a balance, if it does, it needs to transfer it to the upgraded contract through a local upgrade method first
        // Ether can be forcefully sent to any contract though (even if it doesn't have a payable method), so to prevent contracts that need upgrading and for some reason have a balance, use the force method to upgrade them
        if (!_forceEther) {
            require(oldContractAddress.balance == 0);
        }
      
        // Replace the address for the name lookup - contract addresses can be looked up by their name or verified by a reverse address lookup
        Storage.setAddress(keccak256(abi.encodePacked("contract.name", _name)), _upgradedContractAddress);
        // Add the new contract address for a direct verification using the address (used in Storage to verify its a legit contract using only the msg.sender)
        Storage.setAddress(keccak256(abi.encodePacked("contract.address", _upgradedContractAddress)), _upgradedContractAddress);
        // Remove the old contract address verification
        Storage.deleteAddress(keccak256(abi.encodePacked("contract.address", oldContractAddress)));
        // Log it
        emit ContractUpgraded(oldContractAddress, _upgradedContractAddress, now);
    }

    /// @param _name The name of the new contract
    /// @param _contractAddress The address of the new contract
    function addContract(string _name, address _contractAddress) onlyOwner external {
        // Check the contract address
        require(_contractAddress != 0x0);
        // Check the name is not already in use
        address existingContractName = Storage.getAddress(keccak256(abi.encodePacked("contract.name", _name)));
        require(existingContractName == 0x0);
        // Check the address is not already in use
        address existingContractAddress = Storage.getAddress(keccak256(abi.encodePacked("contract.address", _contractAddress)));
        require(existingContractAddress == 0x0);
        // Set contract name and address in storage
        Storage.setAddress(keccak256(abi.encodePacked("contract.name", _name)), _contractAddress);
        Storage.setAddress(keccak256(abi.encodePacked("contract.address", _contractAddress)), _contractAddress);
        // Log it
        emit ContractAdded(_contractAddress, now);
    }


   

}