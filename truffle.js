var HDWalletProvider = require("truffle-hdwallet-provider");
var mnemonic = "key coast book balcony anxiety milk ocean bus science together brain during";

const Web3 = require("web3");
const web3 = new Web3();

module.exports = {
  networks: {
    development: {
      host: "127.0.0.1",
      port: 8545,
      network_id: "*", //match any network id
      gas: 4200000,
      gasPrice: web3.toWei("11", "gwei")
    },

    ropsten: {
         provider: function() {
           return new HDWalletProvider(mnemonic, "https://ropsten.infura.io/v3/dffedbe16fae4f15b402b6192f900002",0)
         },
         network_id: 3,
          gas:   4200000
       }   
  },
   solc: {
    optimizer: {
      enabled: true,
      runs: 200
    }},
    mocha: {
    reporter: 'eth-gas-reporter',
    reporterOptions : {
      currency: 'USD',
      gasPrice: 11
    }
  }
};
