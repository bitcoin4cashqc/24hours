#import web3
from web3 import Web3

from solc import compile_files

import json

import os,time,sys

from multiprocessing.dummy import Pool as ThreadPool 

from decimal import *


import pysqlite3 as sqlite3


import asyncio


import requests




#abi's

#exchange abi
MarketABI =  json.loads('[{"constant":true,"inputs":[{"name":"","type":"address"},{"name":"","type":"uint256"}],"name":"tokenIndexToOwnerOnMarket","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"version","outputs":[{"name":"","type":"uint8"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"USDrate","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"latestUSDUpdate","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"inputs":[{"name":"_digiblyStorageAddress","type":"address"}],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"name":"from","type":"address"},{"indexed":false,"name":"_weiamount","type":"uint256"},{"indexed":false,"name":"_time","type":"uint256"}],"name":"AcceptEth","type":"event"},{"constant":true,"inputs":[{"name":"_tokenId","type":"uint256"},{"name":"ERC721Address","type":"address"}],"name":"getTokenMarketData","outputs":[{"name":"_ownerOnMarket","type":"address"},{"name":"_listed","type":"bool"},{"name":"_price","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_tokenId","type":"uint256"},{"name":"ERC721Address","type":"address"}],"name":"deposit","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_tokenId","type":"uint256"},{"name":"ERC721Address","type":"address"}],"name":"returnToken","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_tokenId","type":"uint256"},{"name":"ERC721Address","type":"address"},{"name":"_price","type":"uint256"}],"name":"listToken","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_tokenId","type":"uint256"},{"name":"ERC721Address","type":"address"}],"name":"unlistToken","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_tokenId","type":"uint256"},{"name":"ERC721Address","type":"address"}],"name":"buyToken","outputs":[],"payable":true,"stateMutability":"payable","type":"function"},{"constant":true,"inputs":[{"name":"_ownerOnMarket","type":"address"},{"name":"ERC721Address","type":"address"}],"name":"tokensOnMarket","outputs":[{"name":"","type":"uint256[]"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_amount","type":"uint256"}],"name":"updateUSD","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[],"name":"acceptEthMoney","outputs":[{"name":"","type":"bool"}],"payable":true,"stateMutability":"payable","type":"function"}]')

#mana abi
collectibleABI = json.loads('[{"constant":true,"inputs":[{"name":"_interfaceID","type":"bytes4"}],"name":"supportsInterface","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"},{"name":"_tokenId","type":"uint256"}],"name":"approve","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"tokenIndexToOwner","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_from","type":"address"},{"name":"_to","type":"address"},{"name":"_tokenId","type":"uint256"}],"name":"transferFrom","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"version","outputs":[{"name":"","type":"uint8"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"thiscontract","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"_tokenId","type":"uint256"}],"name":"ownerOf","outputs":[{"name":"owner","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"_owner","type":"address"}],"name":"balanceOf","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"_owner","type":"address"}],"name":"tokensOfOwner","outputs":[{"name":"","type":"uint256[]"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"tokenIndexToApproved","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"},{"name":"_tokenId","type":"uint256"}],"name":"transfer","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"_who","type":"address"},{"name":"_tokenId","type":"uint256"}],"name":"approvedForExt","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"_tokenId","type":"uint256"}],"name":"getToken","outputs":[{"name":"mintedBy","type":"address"},{"name":"mintedAt","type":"uint64"}],"payable":false,"stateMutability":"view","type":"function"},{"inputs":[{"name":"_digiblyStorageAddress","type":"address"}],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"name":"owner","type":"address"},{"indexed":false,"name":"tokenId","type":"uint256"}],"name":"Mint","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"from","type":"address"},{"indexed":false,"name":"to","type":"address"},{"indexed":false,"name":"tokenId","type":"uint256"}],"name":"Transfer","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"owner","type":"address"},{"indexed":false,"name":"approved","type":"address"},{"indexed":false,"name":"tokenId","type":"uint256"}],"name":"Approval","type":"event"},{"constant":false,"inputs":[{"name":"_owner","type":"address"},{"name":"_name","type":"string"},{"name":"_priceUSD","type":"uint256"}],"name":"mint","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"_tokenId","type":"uint256"}],"name":"getTokenFromStorage","outputs":[{"name":"tokenId","type":"uint256"},{"name":"_name","type":"string"},{"name":"_priceUSD","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"}]')



#general stuff

try:
    conn = sqlite3.connect('data.sqlite3')
    conn.execute('''CREATE TABLE config
                 (name            CHAR(126)     NOT NULL,
                 value            CHAR(126)     NOT NULL);''')
    print ("Config Table created successfully")


except:
    print("Database already there")


conn.close()


def compile_source_file(file_path):
   with open(file_path, 'r') as f:
      source = f.read()

   return compile_source(source)



def wait_for_receipt(w3, tx_hash, poll_interval):
   while True:
       tx_receipt = w3.eth.getTransactionReceipt(tx_hash)
       if tx_receipt:
         return tx_receipt
       time.sleep(poll_interval)



#load config
configDict = {}
print("Loading Config from db...")
conn = sqlite3.connect('data.sqlite3')
configCursor = conn.execute("SELECT name,value FROM config")
all_rows = configCursor.fetchall()
for row in all_rows:
    print('{0} : {1}'.format(row[0], row[1]))
    configDict.update({row[0] : row[1]})

print("Oracle Started:\n")#+str(configDict))

key = configDict["privateKey"]

wallet = configDict["address"]






getcontext().traps[DivisionByZero] = 1
getcontext().traps[Overflow] = 1
getcontext().traps[InvalidOperation] = 1
    


MarketAddress = configDict["marketAddress"]


collectibleAddress = configDict["collectibleAddress"]


w3 = Web3(Web3.HTTPProvider(configDict["web3Provider"]))


MarketAddress = w3.toChecksumAddress(MarketAddress)


collectibleAddress = w3.toChecksumAddress(collectibleAddress)



market_contract = w3.eth.contract(
   address=MarketAddress,
   abi=MarketABI) 

collectible_contract = w3.eth.contract(
   address=collectibleAddress,
   abi=collectibleABI) 


def waitReceipt(sigendraw):
  txn_receipt = None
  count = 0
  while txn_receipt is None and (count < 30):

      txn_receipt = w3.eth.getTransactionReceipt(sigendraw)

      print(txn_receipt)

      time.sleep(10)


  if txn_receipt is None:
      print("executeOrder failed: Timeout")


def updateUSDrate(_newprice,_gasPrice,_gas,wait = False):
    start_timeOfmana = time.time()
    nonce = w3.eth.getTransactionCount(w3.toChecksumAddress(configDict["address"]))  

    # Build a transaction that invokes this contract's function, called transfer
    #ropsten is 3, mainnet is 1
    market_txn = market_contract.functions.updateUSD(_newprice).buildTransaction({
    'gas': int(_gas),
    'gasPrice': w3.toWei(int(_gasPrice), 'gwei'),
    'nonce': nonce,
    })

    


    signed = w3.eth.account.signTransaction(market_txn, key)

    
    

    # When you run sendRawTransaction, you get back the hash of the transaction:
    result = w3.eth.sendRawTransaction(signed.rawTransaction)  
    print("Update USD TX Sent!")
    if wait is True:

      waitReceipt(result)
    print("---Runtime : %s seconds ---" % (time.time() - start_timeOfmana))



def mintCollectible(_owner, _name,_usdprice,_gasPrice,_gas,wait = False):
    start_timeOfmana = time.time()
    nonce = w3.eth.getTransactionCount(w3.toChecksumAddress(configDict["address"]))  

    # Build a transaction that invokes this contract's function, called transfer
    #ropsten is 3, mainnet is 1
    market_txn = collectible_contract.functions.mint(w3.toChecksumAddress(configDict["address"]), "test",int(_usdprice)).buildTransaction({
    'gas': int(_gas),
    'gasPrice': w3.toWei(int(_gasPrice), 'gwei'),
    'nonce': nonce,
    })

    


    signed = w3.eth.account.signTransaction(market_txn, key)

    
    

    # When you run sendRawTransaction, you get back the hash of the transaction:
    result = w3.eth.sendRawTransaction(signed.rawTransaction)  
    print("Mint TX Sent!")
    if wait is True:

      waitReceipt(result)
    print("---Minting runtime : %s seconds ---" % (time.time() - start_timeOfmana))

    

def getTokenFromStorageCall(id):
  idcall = collectible_contract.functions.getTokenFromStorage(id).call()
  return idcall


def ownerOfCall(id):
  idcall = collectible_contract.functions.ownerOf(id).call()
  return idcall 






def handle_event(name,event):

   
    dic = dict(event.args)
    #dic.update({'id': w3.toBytes(event.args["id"])})
    
    
    #data
    dataassetId = event.args["assetId"]
    datapriceInWei = event.args["priceInWei"]
    datapriceInMana = w3.fromWei(datapriceInWei, "ether")

    dataexpiresAt = event.args["expiresAt"]
    datanftAddress = event.args["nftAddress"]

    print(name+" : assetId: "+str(dataassetId)+"\n"+"Mana Price: "+str(datapriceInMana)+" Mana"+"\n"+"Expire @: "+str(dataexpiresAt)+"\n"+"Nft Address : "+str(datanftAddress))#+"\n\n######"+str(dic))


    order = json.dumps(str(dic))

    params = (name,order)

    #conn = sqlite3.connect('data.sqlite3')

    #conn.execute("INSERT INTO events ('eventname','eventdata') \
    #      VALUES (?,?)",params);

    #conn.commit()
    #conn.close()






async def log_loop(dOrderCreatedLoop,poll_interval):
    #get new evetns
    itis = True
    
    while itis is True:
        #os.system('clear')
       
        print("")
        print("")
        #sys.stdout.write("\033[F")
        #sys.stdout.write("\033[F")
        #sys.stdout.write("\033[F")
        start_time = time.time()
        print("["+str(start_time)+"/Block: "+str(w3.eth.getBlock('latest')["number"])+"]looping trought new events")

        #print(str("["+str(start_time)+"/Block: "+str(w3.eth.getBlock('latest')["number"])+"]looping trought new order"))
        

        #conn = sqlite3.connect('data.sqlite3')


        #check mana balance
        manabalance = mana_contract.functions.balanceOf(wallet).call()#.transact()
        print('Getting Mana Balance: '+str(w3.fromWei(manabalance, "ether"))+' MANA')

        allowancebalance = mana_contract.functions.allowance(configDict["address"],MarketAddress).call()
        print('Getting Mana Allowance to market: '+str(w3.fromWei(allowancebalance, "ether"))+' MANA')

        # Wait for transaction to be mined...
        #w3.eth.waitForTransactionReceipt(tx_hash)



        #getting new order 
        #print("looping trought new orders LEGACY")
        #for order in dAuctionCreatedLoop.get_new_entries():
        #    print("looping trought a new order LEGACY")
            #update only status and amount left
            #cursor = conn.execute("SELECT count(*),STATUS,AMOUNTLEFT FROM market WHERE ORDERID = ?", (fulfillEvent["args"]["_id"],))
            #data = cursor.fetchone()
            #if data[0]==1:
            #    conn.execute("UPDATE market set STATUS = ?, AMOUNTLEFT = ?  where ORDERID = ?", ("fulfilled",0,fulfillEvent["args"]["_id"]))
        #    handle_event("AuctionCreated",order)
            #itis = False

        #getting new order 
        #print("looping trought new orders")
        for order in dOrderCreatedLoop.get_new_entries():
            print("looping trought a new order")
            #update only status and amount left
            #cursor = conn.execute("SELECT count(*),STATUS,AMOUNTLEFT FROM market WHERE ORDERID = ?", (fulfillEvent["args"]["_id"],))
            #data = cursor.fetchone()
            #if data[0]==1:
            #    conn.execute("UPDATE market set STATUS = ?, AMOUNTLEFT = ?  where ORDERID = ?", ("fulfilled",0,fulfillEvent["args"]["_id"]))
            handle_event("OrderCreated",order,manabalance,allowancebalance)
            #itis = False




                
                
                

        #conn.commit()
        #conn.close()

        #print("Listening all news events done!")

        print("---LISTEN RUNTIME: %s seconds ---" % (time.time() - start_time))


        await asyncio.sleep(poll_interval)




def loopIt():
    OrderCreatedLoop = market_contract.events.OrderCreated.createFilter(fromBlock ='latest')

    #AuctionCreatedLoop = market_contract.events.AuctionCreated.createFilter(fromBlock ='latest')


    #OrderSuccessfulLoop = market_contract.events.OrderSuccessful.createFilter(fromBlock ='latest')

    #OrderCancelledLoop = market_contract.events.OrderCancelled.createFilter(fromBlock ='latest')

  
    #loop the proces

    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(
            asyncio.gather(
                log_loop(OrderCreatedLoop, 3)))
    except Exception as exc:
        print(str(exc))
        time.sleep(3)
        
        loopIt()

    else:
        loop.close()