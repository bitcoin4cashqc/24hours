from botImports import *


#get latest ETH/USD ticker from kraken
r = requests.get('https://api.kraken.com/0/public/Ticker?pair=ETHUSD')
data = r.json() # Check the JSON Response Content documentation below
print("ETH/USD rate: "+str(data["result"]["XETHZUSD"]["b"][0])+" USD")


rounded = str(round(float(data["result"]["XETHZUSD"]["b"][0]),2))
print("ROUNDED ETH/USD rate: "+rounded+" USD")


corrected = rounded.replace(".", "")

print("CORRECTED DECIMAL ETH/USD rate: "+corrected+" USD")




#updated the smart contract rate

updateUSDrate(int(corrected),configDict["gas"],configDict["gasLimit"],True)


