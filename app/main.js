
App = {
  web3Provider: null,
  contracts: {},

 
  

  init: function() {
    return App.initWeb3();
  },

  initWeb3: function() {
    // Initialize web3 and set the provider to the testRPC.
    if (typeof web3 !== 'undefined') {
      App.web3Provider = web3.currentProvider;
      web3 = new Web3(web3.currentProvider);
    } else {
      // set the provider you want from Web3.providers
      App.web3Provider = new Web3.providers.HttpProvider('http://localhost:8545');
      web3 = new Web3(App.web3Provider);
      toastr.warning('You need MetaMask extension or Parity to use this app.');

    }

    return App.initContract();
  },

  initContract: function() {
    $.getJSON('Main.json', function(data) {
      // Get the necessary contract artifact file and instantiate it with truffle-contract.
      var MainArtifact = data;

      try {
         App.contracts.Main = TruffleContract(MainArtifact);

        // Set the provider for our contract.
        App.contracts.Main.setProvider(App.web3Provider);

        App.checkAccount();
        App.MainApp();
      } catch(err) {
          console.log(err);
      }

    });
 
    return App.bindEvents();
  },

  bindEvents: function() {
     $(document).on('click', '#slotMachineButtonShuffle', App.startRoll);
     $(document).on('click', '#whitdraw', App.whitdraw);
  },

 



  checkAccount: function() {
    web3.eth.getAccounts(function(error, accounts) {
        App.account = accounts[0];

        App.contracts.Main.deployed().then(function(_instance) {
            instance = _instance;

          
            //App.checkBalance();
        })
        .catch(function(err) {
            toastr.warning('Make sure you are connected to Ropsten network: '+err);
        });
    });
  },

   checkBalance: function() {

    instance.balanceOf.call(App.account).then(function(_balance) {

        App.balance = _balance.valueOf();

        var balanceInEther = web3.fromWei(App.balance, "ether");

        $("#balance").text(balanceInEther + " ether");
        
    });
   },
       

 

  MainApp: function() {

    

        App.started = 0;
        console.log("app started")


  }

};

$(function() {
  $(window).load(function() {
    App.init();
  });
});
