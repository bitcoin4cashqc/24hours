'use strict';


//const assert = require('assert');




const Storage = artifacts.require('./Storage');
const Upgrade = artifacts.require('./Upgrade');
const Role = artifacts.require('./Role');
const Settings = artifacts.require('./Settings');
const Main = artifacts.require('./Main');

var acc1 = 0x0
var acc2 = 0x0

function makeseed(origin, iternum) {
  var seed = origin.toString(16);
  for (var i = 0; i < iternum; i++) {
    seed = web3.sha3(seed,{encoding:'hex'});
  }
  return seed;
}




async function assertRevert (promise) {
  try {
    await promise;
  } catch (error) {
    ////console.log('revert', `Expected "revert", got ${error} instead`);
    return `Expected "revert", got ${error} instead`;
  }
  should.fail('Expected revert not received');
}


// Since all of our testing functions are async, we store the
// contract instance at a higher level to enable access from
// all functions
//var StorageInstance;
//var UpgradeInstance;




contract('Storage', async (accounts) => {



  it("should create Storage", async function () {

    let StorageInstance = await Storage.deployed();
    

  });

  it("should write in Storage as owner", async function () {

    let StorageInstance = await Storage.deployed();
    let write = await StorageInstance.setBool("Yo",true);
    
  });

  it("should read back from Storage as owner", async function () {

    let StorageInstance = await Storage.deployed();
    let read = await StorageInstance.getBool.call("Yo");
    assert.strictEqual(read, true);
    

  });


    


})






contract('Role', async (accounts) => {

  it("should create Role", async function () {
    
    let RoleInstance = await Role.deployed();

  });


})



contract('Settings', async (accounts) => {

  it("should create Settings", async function () {
    
    let SettingsInstance = await Settings.deployed();

  });

  it("should return true calling isRole owner on the current address", async function () {
    
    let SettingsInstance = await Settings.deployed();
    let read = await SettingsInstance.isRole.call(accounts[0], "owner");
    assert.strictEqual(read, true);

    acc1 = accounts[0]
    acc2 = accounts[1]

    console.log("acc1: "+acc1)
    console.log("acc2: "+acc2)


  });

  

})




contract('Main', async (accounts) => {

  it("should create Main", async function () {
    
    let MainInstance = await Main.deployed();

  });


})







contract('Upgrade', async (accounts) => {




  it("should create Storage", async function () {
    
    let UpgradeInstance = await Upgrade.deployed();

  });

  it("should create link all the contracts to  Storage", async function () {
    
    let UpgradeInstance = await Upgrade.deployed();

  });



})



var actualbl = 0
describe("it should connect everything and init", async function (){


it("should upgrade all the contracts to  Storage", async function () {
  actualbl = web3.eth.getBalance(acc1);
  console.log("Actual balance = "+web3.fromWei(actualbl, "ether" )+" ETH")
    
    let StorageInstance = await Storage.deployed();
    let RoleInstance = await Role.deployed();
    let SettingsInstance = await Settings.deployed();
    let MainInstance = await Main.deployed();
 
    let UpgradeInstance = await Upgrade.deployed();

    assert(UpgradeInstance.addContract("Role",RoleInstance.address));
    assert(UpgradeInstance.addContract("Settings",SettingsInstance.address));
    assert(UpgradeInstance.addContract("Main",MainInstance.address));
  
    assert(UpgradeInstance.addContract("Upgrade",UpgradeInstance.address));


  });


it("should run init() from Settings, which lock storage except for last upgraded contracts", async function () {

  let SettingsInstance = await Settings.deployed();
  assert(SettingsInstance.init());


  });


it("should retry to write in storage as owner and not being able to", async function () {

  let StorageInstance = await Storage.deployed();
  let test = await assertRevert(StorageInstance.setBool("Oy",true))
  let read = await StorageInstance.getBool.call("Oy");
  assert.strictEqual(read, false);


  });





});



